//
// Created by Patryk Kaczmarek
//
#include <vector>
#include <queue>
#include <thread>
#include <mutex>
#include <functional>


#ifndef SCR_MULTITHREADING_SIMPLETHREADPOOL_HPP
#define SCR_MULTITHREADING_SIMPLETHREADPOOL_HPP

class SimpleThreadPool{
public:
    SimpleThreadPool(size_t siz = std::thread::hardware_concurrency());

    template <class Function, class... Args>
    auto addTask(Function&& f, Args&&... args) ->
    std::future<typename std::result_of<Function(Args...)>::type>;

    ~SimpleThreadPool();
private:
    std::vector<std::thread> threads_;
    std::queue<std::function<void()>> tasks_;

    std::condition_variable cond;
    std::mutex mutex_;
    std::atomic_bool is_done_;

};

SimpleThreadPool::SimpleThreadPool(size_t siz) : is_done_(false) {
    for(size_t i = 0; i<siz;i++) {
        threads_.push_back(std::move(std::thread([this]() {
            for(;;) {
                std::function<void()> task;
                {
                    std::unique_lock<std::mutex> lock(this->mutex_);
                    if(this->is_done_ == true)
                        return;
                    this->cond.wait(lock, [this]
                    {return this->is_done_ || not this->tasks_.empty();});
                    if(not this->tasks_.empty() && this->is_done_ == false) {
                        task = std::move(this->tasks_.front());
                        this->tasks_.pop();
                        task();
                    }
                }

            }
        })));
    }
}


template <class Function, class... Args>
auto SimpleThreadPool::addTask(Function&& f, Args&&... args) ->
std::future<typename std::result_of<Function(Args...)>::type> {
    using return_type = typename std::result_of<Function(Args...)>::type;

    auto task = std::make_shared<std::packaged_task<return_type()>>
                                                                 (std::bind(std::forward<Function>(f), std::forward<Args>(args)...));
    std::future<return_type> ret = task->get_future();
    {
        std::unique_lock<std::mutex> lock(mutex_);
        if(is_done_) {
            throw std::runtime_error("Adding tasks to stopped ThreadPool is forbidden!");
        }
        tasks_.emplace([task]{
            (*task)();
        });
    }
    cond.notify_one();
    return ret;
}

SimpleThreadPool::~SimpleThreadPool() {
    is_done_ = true;
    cond.notify_all();
    for (std::thread &thread_ : threads_) {
        thread_.join();
    }
}

#endif //SCR_MULTITHREADING_SIMPLETHREADPOOL_HPP
