#include <iostream>
#include <vector>
#include <chrono>

#include "ThreadPool.h"
#include "SimpleThreadPool.hpp"

using namespace std;

void printStuff() {
    std::cout<<"Print some wierd stuff" <<std::endl;
}
int printStuff2(int i = 1) {
    std::cout<<"Print some wierder stuff, :i= "<<i <<std::endl;
    return i;
}


int main()
{
//
//    ThreadPoolSCR pool(4);
//    std::vector< std::future<int> > results;
//
//    for(int i = 1; i <5; ++i) {
//        results.emplace_back(
//                pool.enqueue([i] {
//                    std::cout<<"Strarting thread "<<i<<std::endl;
//                    return i*i*i;
//                })
//        );
//    }
//
//    std::this_thread::sleep_for(std::chrono::seconds(10));
//    for(auto && result: results)
//        std::cout << result.get() << ' ';
//    std::cout << std::endl;


    SimpleThreadPool stp;
    stp.addTask(printStuff);
    auto ret = stp.addTask(printStuff2,1);
    cout<<"Ret is :"<<ret.get()<<endl;

    cout<<"DOne"<<endl;
    cin.get();
    return 0;
}